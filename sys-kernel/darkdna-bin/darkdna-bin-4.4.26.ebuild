# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

DESCRIPTION="Custom binary kernel distribution for DarkDNA"
HOMEPAGE="https://darkdna.net/"
SRC_URI="https://m.ddna.co/ddna-dl/${P}.tar.gz"
LICENSE="BSD"

SLOT="4.4.26"
KEYWORDS="amd64"
IUSE=""

DEPEND="sys-boot/grub"

KERNVERS="4.4.26-gentoo"

src_install() {
	insinto /boot
	doins config-${KERNVERS}
	doins initramfs-genkernel-x86_64-${KERNVERS}
	doins System.map-${KERNVERS}
	doins vmlinuz-${KERNVERS}
}

pkg_preinst() {
	mount /boot
}

pkg_postinst() {
	/usr/sbin/grub-mkconfig -o /boot/grub/grub.cfg
	umount /boot
}
