# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

DESCRIPTION="Custom message bus for DarkDNA"
HOMEPAGE="https://darkdna.net/"
SRC_URI="https://m.ddna.co/ddna-dl/${P}.tar.gz"
LICENSE="BSD"

SLOT="0"
KEYWORDS="amd64"
IUSE=""

src_install() {
	dobin birman
	dobin birman-send
	dobin birman-cat
	doinitd init.d/birman
}
